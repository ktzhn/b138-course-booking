const User = require("../model/User");
const Course = require ("../model/Course");
// npm install bcrypt
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// User Registration
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object usign the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

/*2. Create a getProfile controller method for retrieving the details of the user:
a. Find the document in the database using the user's ID
b. Reassign the password of the returned document to an empty string
c. Return the result back to the frontend*/

module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {
		if(result == null){
			return false;
		}
		else{
			result.password = "";
			return result;
		}
	})
}

// Enroll user to a class
// Async await will be used in enrolling the user because we will need to update two separate documents when enrolling a user
module.exports.enroll = async (usrData, data, reqParams) => {
	
	// Add the course ID in the enrollments array of the user
	let isUserUpdated = await User.findById(usrData.id).then(user => {
		// Adds the courseId in the user's enrollment array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Add the user ID in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : usrData.id});

		// Save the updated course information in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	else{
		return false;
	}
}
